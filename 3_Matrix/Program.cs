﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _3_Matrix
{
    static class Program
    {
        static void Main()
        {
            int n;
            int m;
            try
            {
                InputData(out n, out m);
            }
            catch (Exception e)
            {
                WriteLine(e);
                ReadKey();
                return;
            }

            int[,] matrix = new int[n, m];
            List<int> indexes = new List<int>();
            WriteLine("Generated matrix:");

            //для перевірки
            //matrix = Init2();
            InitFirst(matrix, indexes);

            Task1(matrix, indexes);
            Task2(matrix);

            ReadKey();
        }

        private static void InputData(out int n, out int m)
        {
            WriteLine("Enter rowcount of matrix");
            n = int.Parse(ReadLine());
            WriteLine("Enter colcount of matrix");
            m = int.Parse(ReadLine());
        }

        private static void InitFirst(int[,] matrix, List<int> indexes)
        {
            Random generator = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < matrix.GetLength(0); i++, WriteLine())
            {
                for (int j = 0; j < matrix.GetLength(1); j++, Write("\t"))
                {
                    matrix[i, j] = generator.Next(20) - 5;
                    Write(matrix[i, j]);
                    //для другого завдання, пошук рядків, де присутній хоч 1 відємний елемент
                    if (!indexes.Contains(i) && matrix[i, j] < 0)
                    {
                        indexes.Add(i);
                    }
                }
            }
        }

        private static int[,] Init2() => new int[,] {{2, 3, 5, 2}, {2, 4, 6, 2}, {-2, 7, 2, 0}};

        private static void Task1(int[,] matrix, List<int> indexes)
        {
            WriteLine("Task#1 сума по рядках, де є хоч 1 від'ємний елемент");
            foreach (var i in indexes)
            {
                var sum = 0;
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
                WriteLine($"Сума {i + 1} рядка : {sum}");
            }
        }

        private static void Task2(int[,] matrix)
        {
            WriteLine("Task#2 iндекси сiдлових точкок:");
            //найменше у рядку
            var LowerInRow = new int[matrix.GetLength(0)];
            //найбільше по стовпцях
            var BiggerInCol = new int[matrix.GetLength(1)];

            LowerInRow = LowerInRow.Select(x => int.MaxValue).ToArray();
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < LowerInRow[i])
                    {
                        LowerInRow[i] = matrix[i, j];
                    }
                    if (matrix[i, j] > BiggerInCol[j])
                    {
                        BiggerInCol[j] = matrix[i, j];
                    }
                }
            }
            for (var i = 0; i < LowerInRow.Length; i++)
            {
                for (var j = 0; j < BiggerInCol.Length; j++)
                    if (LowerInRow[i] == BiggerInCol[j])
                        WriteLine($"{LowerInRow[i]}({i + 1},{j + 1})");
            }
        }
    }
}
