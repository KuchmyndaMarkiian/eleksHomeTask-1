﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System;
using System.Collections.Generic;
using static System.Console;

namespace _4_Matrix
{
   static class Program
    {
        static void Main()
        {
            int[,] matrix;
            matrix = new int[8, 8];
            /* для перевірки*/
            //matrix = Init2();
            //matrix = Init3();
            List<int> indexes = new List<int>();
            Generate(matrix, indexes);
            Task1(matrix);
            Task2(matrix, indexes);

            ReadKey();
        }

        private static void Task2(int[,] matrix, List<int> indexes)
        {
            WriteLine("Task#2 сума по рядках, де є хоч 1 вiд'ємний елемент");
            foreach (var i in indexes)
            {
                int sum = 0;
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
                WriteLine($"Сума {i + 1} рядка : {sum}");
            }
        }

        private static void Task1(int[,] matrix)
        {
            WriteLine("Task#1 знайти k при яких k-ий рядок спiвпадає з k-им стовпцем");
            int k = -1;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int tmp = 0;
                    if (matrix[i, j] == matrix[j, i])
                    {
                        tmp++;
                    }
                    if (tmp == matrix.GetLength(0))
                    {
                        k = i + 1;
                        break;
                    }
                }
            }
            WriteLine($"Answer:{(k < 0 ? "nope" : k.ToString())}");
        }

        private static void Generate(int[,] matrix, List<int> indexes)
        {
            Random generator = new Random(DateTime.Now.Millisecond);
            WriteLine("Generated matrix:");
            for (int i = 0; i < matrix.GetLength(0); i++, WriteLine())
            {
                for (int j = 0; j < matrix.GetLength(1); j++, Write("\t"))
                {
                    matrix[i, j] = generator.Next(20) - 5;
                    Write(matrix[i, j]); //рандомний перебір значень
                    if (!indexes.Contains(i) && matrix[i, j] < 0)
                        //для другого завдання, пошук рядків, де присутній хоч 1 відємний елемент
                    {
                        indexes.Add(i);
                    }
                }
            }
        }

        private static int[,] Init2()
        {
            return new[,]
            {
                {1, 4, 5, 6, 2, 1, 3, 4}, {1, 1, 9, 2, 2, 3, 8, 7}, {-6, 0, 12, 3, 8, 9, 6, 3},
                {6, 2, 3, 0, 1, 7, 9, 8}, {2, 2, 2, 1, 6, 5, 4, 7}, {9, 6, 3, 7, 5, 5, 5, 6},
                {7, -7, -7, 9, 3, 3, 3, 2}, {0, 3, 2, 8, 5, 7, 8, 9}
            };
        }

        private static int[,] Init3()
        {
            return new[,]
            {
                {-3, 2, 3, 4, 5}, {2, 1, 1, -2, 1}, {-1, 1, 5, 0, 1}, {4, -2, 0, 1, -3}, {1, 1, 1, 3, 1}
            };
        }
    }
}
