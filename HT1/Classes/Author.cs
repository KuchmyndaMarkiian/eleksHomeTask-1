using System;
using HT1.Interfaces;
using Other;

namespace HT1.Datas
{
    public class Author : IAuthor, IComparable<Author>
    {
        public string Name { get; set; }
        public Book[] Books { get; set; }

        public Author(string name, Book[] books)
        {
            Name = name;
            Books = books;
        }

        public int GetBookCount()
        {
            return Books.Length;
        }

        public int CompareTo(Author other)
        {
            if (GetBookCount() > other.GetBookCount())
            { return 1;}
            if (GetBookCount() < other.GetBookCount())
            { return -1;}
            return 0;
        }

        public override string ToString() => $"Author- {Name}:\n{Books.toString()}\n";
    }
}