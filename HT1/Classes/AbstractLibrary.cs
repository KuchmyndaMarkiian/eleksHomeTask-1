﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System;
using System.Linq;
using HT1.Datas;
using HT1.Interfaces;

namespace Library.Classes
{
    abstract class AbstractLibrary : IComparable<AbstractLibrary>, ICountingBooks
    {
        private readonly string name;
        public string Name => name;

        public Section[] Sections { get; set; }

        public override string ToString() => "Abstract library.";

        protected AbstractLibrary(string name, Section[] sections)
        {
            this.name = name;
            this.Sections = sections;
        }

        protected AbstractLibrary()
        {
        }

        public int CompareTo(AbstractLibrary other) => 0;

        public int GetBookCount()
            => Sections.SelectMany(sections => sections.Authors).Sum(authors => authors.Books.Length);

        //Additional task#1 Author which wrote more books
        public Author GetAuthorWithMoreBooks()
            =>
                Sections.Select(s => s.Authors.First(a => a.GetBookCount() == s.Authors.Max(a1 => a1.GetBookCount())))
                    .Last();

        //Additional task#2 Section which has more books
        public Section GetSectionWithMoreBooks()
            => Sections.First(s => s.GetBookCount() == Sections.Max(s1 => s1.GetBookCount()));

        //Additional task#3 Book which has less pages
        public Book GetBookWithLessPages()
        {
            Book result = new Book(null, int.MaxValue);
            foreach (var section in Sections)
            {
                foreach (var author in section.Authors)
                {
                    foreach (var book in author.Books)
                    {
                        if (book.CompareTo(result) < 0)
                        {
                            result = book;
                        }
                    }
                }
            }
            return result;
        }
    }
}
