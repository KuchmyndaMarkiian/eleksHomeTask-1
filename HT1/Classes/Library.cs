﻿using Library.Classes;
using Other;

namespace HT1.Datas
{
    class Library : AbstractLibrary
    {
        public Library() 
        {
        }

        public Library(string name, Section[] sections) : base(name, sections)
        {
        }

        public override string ToString() => $"Library \"{Name}\" with sections::\n\n{Sections.toString()}\n";
    }
}
