using System;
using HT1.Interfaces;
using _1_Library.Interfaces;

namespace HT1.Datas
{
    public class Book : IBook, IComparable<Book>
    {
        public string Name { get; set; }

        public int Pages { get; set; }

        public Book()
        {
        }

        public Book(string name, int pages)
        {
            this.Name = name;
            this.Pages = pages;
        }

        public override string ToString() => $"\'{Name}\', {Pages}p.";

        public int CompareTo(Book other)
        {
            if (Pages > other.Pages)
                return 1;
            if (Pages < other.Pages)
                return -1;
            return 0;
        }
    }
}