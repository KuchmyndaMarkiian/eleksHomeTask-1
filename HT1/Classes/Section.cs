using System;
using System.Linq;
using HT1.Interfaces;
using Other;

namespace HT1.Datas
{
    internal class Section : ISection, IComparable<Section>
    {
        public string Name { get; set; }

        public Author[] Authors { get; set; }

        public Section()
        {
        }

        public Section(string name, Author[] authors)
        {
            this.Name = name;
            this.Authors = authors;
        }

        public override string ToString() => $"Section {Name} which contains books such as:\n{Authors.toString()}\n";
        public int GetBookCount() => Authors.Select(a => a.Books.Length).Sum();

        public int CompareTo(Section other)
        {
            if (GetBookCount() > other.GetBookCount())
            { return 1;}
            if (GetBookCount() < other.GetBookCount())
            {  return -1;}
            return 0;
        }
    }
}