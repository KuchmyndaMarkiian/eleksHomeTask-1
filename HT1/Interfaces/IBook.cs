using _1_Library.Interfaces;

namespace HT1.Interfaces
{
    public interface IBook: ICommonType
    {
        string Name { get; set; }
        int Pages { get; set; }
    }
}