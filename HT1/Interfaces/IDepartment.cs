﻿using HT1.Datas;
using _1_Library.Interfaces;

namespace HT1.Interfaces
{
    public interface ISection:ICountingBooks, ICommonType
    {
        string Name { get; set; }
        Author[] Authors { get; set; }
    }
}
