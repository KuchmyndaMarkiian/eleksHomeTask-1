﻿using System.Linq;
using _1_Library.Interfaces;

namespace Other
{
    public  static class Extension
    {
        public static string toString(this ICommonType[] array)
            => array.Select(x => x.ToString()).Aggregate((f, s) => $"{f}\n{s}");
    }
}
