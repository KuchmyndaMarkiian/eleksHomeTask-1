﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace _2_Matrix
{
    class Program
    {
        static void Main()
        {
            int n;
            int m;
            try
            {
                InputData(out n, out m);
            }
            catch (Exception e)
            {
                WriteLine(e);
                ReadKey();
                return;
            }

            int[,] matrix = new int[n, m];
            Random generator = new Random(DateTime.Now.Millisecond);

            WriteLine("Generated matrix:");
            Init1(matrix, generator);

            Task1(matrix);
            Task2(matrix);

            ReadKey();
        }

        private static void Task2(int[,] matrix)
        {
            WriteLine(
                "Task#2 розташувати рядки заданої матрицi по зростанню кiлькостi однакових елементiв у кожному рядку");
            /*масив Number(число, повторюванісь, № рядка)*/
            CNumber[] order = new CNumber[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                var tmpArray = new List<CNumber>();
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int tmp = matrix[i, j];
                    if (!tmpArray.Any(x => x.Number == tmp))
                    {
                        tmpArray.Add(new CNumber {Count = 1,  Number= tmp, IndexRow = i});
                    }
                    else
                    {
                        tmpArray.First(x => x.Number == tmp).Increment();
                    }
                }
                order[i] = new CNumber {Count = tmpArray.Max(x => x.Count), IndexRow = i};
            }
            var tmp1 = order.ToList();
            tmp1.Sort();
            foreach (var VARIABLE in tmp1)
            {
                for (int i = 0; i < matrix.GetLength(1); i++)
                {
                    Write(matrix[VARIABLE.IndexRow, i] + "\t");
                }
            }
            WriteLine();

        }

        private static void Task1(int[,] matrix)
        {
            WriteLine("Task#1 визначити номер першого зi стовпцiв, який не мiстить нi одного вiд'ємного елемента");
            int index = -1;
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                int tmp = 0;
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (matrix[j, i] < 0)
                    {
                        tmp++;
                    }
                }
                if (tmp == 0)
                {
                    index = i + 1;
                    break;
                }
            }
            WriteLine($"Answer : {(index > 0 ? index.ToString() : "nope")}");
        }

        private static void Init1(int[,] matrix, Random generator)
        {
            for (int i = 0; i < matrix.GetLength(0); i++, WriteLine())
            {
                for (int j = 0; j < matrix.GetLength(1); j++, Write("\t"))
                {
                    matrix[i, j] = generator.Next(20) - 2;
                    Write(matrix[i, j]);
                }
            }
        }

        private static void InputData(out int n, out int m)
        {
            WriteLine("Enter rowcount of matrix");
            n = int.Parse(ReadLine());
            WriteLine("Enter colcount of matrix");
            m = int.Parse(ReadLine());
        }

        private class CNumber : IComparable<CNumber>
        {
            public int IndexRow { get; set; }
            public int Number { get; set; }
            public int Count { get; set; }

            public void Increment()
            {
                Count++;
            }

            //для сортування рядків по к-сті однакових елементів
            public int CompareTo(CNumber other)
            {
                if (Count > other.Count)
                {
                    return 1;
                }
                if (Count < other.Count)
                {
                    return -1;
                }
                return 0;
            }
        }
    }
}
